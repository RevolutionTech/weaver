﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float speed;

	private Rigidbody rb;

	void Start () {
		rb = GetComponent<Rigidbody> ();
	}

	void FixedUpdate () {
		// Get keyboard input
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		// Move player
		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		rb.transform.Translate (movement * speed);
	}
}
